# -*- coding: utf-8 -*-
import http
import json
import logging
import os
import re
import sys
import traceback
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple, Union
from urllib.parse import parse_qs, unquote, urlparse

import dotenv
import requests
from orkg import ORKG
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

# Add root directory to Python path
# flake8: noqa: E402
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))
CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
ENV_FILE = os.path.join(CURRENT_DIR, "..", ".env")
LOG_FILE = os.path.join(CURRENT_DIR, "..", f"log_{datetime.today().strftime('%Y-%m-%d')}.txt")

dotenv.load_dotenv(ENV_FILE, override=True)

from _migration.src.models import destination_models, source_models
from app.services.common.orkg_backend import OrkgBackendWrapperService
from app.services.contribution import ContributionComparisonService
from app.services.contribution.comparison import _common

level = logging.getLevelName("DEBUG")

logger = logging.getLogger(__name__)
logger.setLevel(level=level)

stdout = logging.StreamHandler()
stdout.setLevel(level=level)

formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s: %(message)s")
stdout.setFormatter(formatter)

file_handler = logging.FileHandler(LOG_FILE, mode="w")
file_handler.setFormatter(formatter)
file_handler.setLevel(level=level)

logger.addHandler(stdout)
logger.addHandler(file_handler)

# globally defined lookup objects to avoid duplicate creations/calls
orkg = ORKG(host=os.getenv("ORKG_BACKEND_API_HOST"))
migrated_response_hashes = {}
orkg_resources = {}


class LinksIssues:
    LINK_SCHEMA_INCORRECT = "Link schema incorrect for link: {}", []
    LINK_HAS_NO_COMPARISON_DATA = "No comparison data found for link: {}", []
    LINK_POINTS_TO_MULTIPLE_THINGS = (
        "Link {} points to duplicate things. Creating new one: {}",
        [],
    )
    LINK_POINTS_TO_UNPUBLISHED_COMPARISON = "Link {} points to unpublished comparison.", []


class VisualizationIssues:
    THING_COULD_NOT_BE_FOUND_IN_THE_GRAPH = "Couldn't find thing: {}", []
    ERROR_WHILE_RETRIEVING_THING_FROM_ORKG = "Issue happened while calling ORKG for thing: {}", []
    THING_TYPE_UNDETECTED = "Couldn't detect thing type for thing: {}", []
    THING_DUPLICATED = "Duplicated thing: {}", []
    THING_CONFIG_CANNOT_BE_PARSED = "Error happened while parsing config of: {}. Error : {}", []
    THING_HAS_NO_RESPONSE_HASH = "response_hash not found for thing: {}. Recomputing it.", []
    THING_HAS_NO_COMPARISON_DATA = "No comparison data found for thing: {}", []


class ISSUES(LinksIssues, VisualizationIssues):
    ORKG_BACKEND_EXCEPTION = "An error occurred while calling the ORKG backend, Exception: {}", []
    UNEXPECTED_EXCEPTION = "An unexpected exception occurred. Skipping for now. Exception: {}", []
    UNKNOWN_CONFIG_FIELD_TYPE = "Unknown config field type(s) for values ({}, {})", []
    PROBLEM_WHILE_ADAPTING_COMPARISON_SCHEMA = (
        "Problem occurred while adapting comparison schema for data {}. Recomputing it.",
        [],
    )
    PROBLEM_WHILE_RECOMPUTING_COMPARISON = (
        "Error: {} occurred while recomputing comparison for config {}.",
        [],
    )
    DIFFERENT_CONFIGS_WHILE_RECOMPUTING_COMPARISON = (
        "Given config {} and output comparison {} are different.",
        [],
    )


def warning(issue: Tuple[str, List[str]], to_append: Any, *args):
    logger.warning(issue[0].format(*args))
    issue[1].append(to_append)


def report(table, n_entries):
    logger.info(
        f"Migration report of table '{table['table_name']}'. "
        f"Total number of rows to be migrated is: {n_entries}. Following issues occurred:"
    )

    for category in [table["issues"], ISSUES]:
        for attr in vars(category):
            if "__" in attr:
                continue

            issue = getattr(category, attr)
            logger.warning(
                f"\t\tIssue '{attr}' occurred {len(issue[1])} times. Please check the following IDs: '{issue[1]}'"
            )


def to_bool(str_or_bool: Union[str, bool], default: Any):
    if str_or_bool is None:
        return default

    if isinstance(str_or_bool, bool):
        return str_or_bool

    if isinstance(str_or_bool, str) and not str_or_bool:
        return default

    return str_or_bool.lower() == "true"


def str_to_list(string: str, default: Any):
    if not string:
        return default

    return string.split(",")


def create_db_connection(url: str, base, logging_name: str):
    engine = create_engine(url, echo=True, logging_name=logging_name)
    session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

    if logging_name == "source":
        # dropping the public schema should have been happened prior to importing the DB dump,
        # i.e. before calling this script.
        base.metadata.reflect(bind=engine)
    elif logging_name == "destination":
        base.metadata.drop_all(bind=engine)
        base.metadata.create_all(bind=engine, checkfirst=True)

    return session()


def detect_thing_type(thing_classes: List[str]):
    if "Visualization" in thing_classes:
        return "VISUALIZATION"
    if "Comparison" in thing_classes:
        return "COMPARISON"
    if "ComparisonDraft" in thing_classes:
        return "DRAFT_COMPARISON"
    if "Diagram" in thing_classes:
        return "DIAGRAM"
    if "LiteratureListPublished" in thing_classes:
        return "LIST"
    if "SmartReviewPublished" in thing_classes:
        return "REVIEW"
    if "PaperVersion" in thing_classes:
        return "PAPER_VERSION"
    if "QualityReview" in thing_classes:
        return "QUALITY_REVIEW"

    return None


def parse_config(url: str, is_link=False) -> Tuple[Optional[str], Optional[dict]]:
    if is_link:
        comparison_id = re.search(r"/comparison/(R\d+).*", url)

        if comparison_id:
            comparison_id = comparison_id.group(1)
            return comparison_id, None

    parsed_query = parse_qs(urlparse(url).query)

    if is_link:
        contributions = str_to_list(parsed_query.get("contributions", [None])[0], default=[])
    else:
        # will raise KeyError if key doesn't exist.
        contributions = str_to_list(parsed_query["contributions"][0], default=[])

    predicates = (
        [
            unquote(item)
            for item in str_to_list(
                url[url.find("&properties=") + 12 : url.find("&", url.find("&properties=") + 1)],
                default=[],
            )
        ]
        if "properties" in parsed_query
        else []
    )

    comparison_type = parsed_query["type"][0].upper() if "type" in parsed_query else None

    return None, {
        "predicates": predicates,
        "contributions": contributions,
        "transpose": to_bool(parsed_query.get("transpose", [None])[0], default=False),
        "response_hash": parsed_query.get("response_hash", [None])[0],
        "type": comparison_type,
    }


def recompute_comparison(config: Dict[str, Any], source_entity) -> Optional[Dict[str, Any]]:
    orkg_backend = next(OrkgBackendWrapperService.get_instance())
    service = ContributionComparisonService(orkg_backend)

    if config["type"] is None:
        config["type"] = "MERGE"

    try:
        comparison = service.compare(
            contribution_ids=config["contributions"],
            comparison_type=config["type"],
            format=None,
        )["comparison"]
    except Exception as e:
        warning(
            ISSUES.PROBLEM_WHILE_RECOMPUTING_COMPARISON,
            (source_entity.id, e),
            e,
            config,
        )
        traceback.print_exc()
        return None

    if len(comparison.contributions) != len(config["contributions"]):
        warning(
            ISSUES.DIFFERENT_CONFIGS_WHILE_RECOMPUTING_COMPARISON,
            source_entity.id,
            config,
            comparison.dict(),
        )
        config["contributions"] = [c.id for c in comparison.contributions]

    return comparison.dict()


def recompute_thing(config: Dict[str, Any], source_thing) -> Optional[destination_models.Thing]:
    comparison = recompute_comparison(config, source_thing)

    if not comparison:
        return None

    return destination_models.Thing(
        created_at=source_thing.created_at,
        updated_at=source_thing.updated_at,
        thing_type="COMPARISON",
        thing_key=source_thing.resource_id,
        data=comparison,
        config=config,
    )


def get_classes(resource_id: str) -> List[str]:
    if resource_id.startswith("L"):
        return []

    if resource_id not in orkg_resources:
        try:
            orkg_resource = orkg.resources.by_id(resource_id)
        except requests.exceptions.ConnectionError:
            return []

        if orkg_resource.status_code != http.HTTPStatus.OK:
            return []

        orkg_resources[resource_id] = orkg_resource

    if (
        not orkg_resources[resource_id].content
        or "classes" not in orkg_resources[resource_id].content
    ):
        return []

    return _common.clean_classes(orkg_resources[resource_id].content["classes"])


def get_path_labels(path: List[str]) -> List[str]:
    path_labels = []

    for resource_id in path:
        if resource_id not in orkg_resources:
            try:
                if resource_id.startswith("R"):
                    orkg_resource = orkg.resources.by_id(resource_id)
                else:
                    orkg_resource = orkg.predicates.by_id(resource_id)
            except requests.exceptions.ConnectionError:
                continue

            if orkg_resource.status_code != http.HTTPStatus.OK:
                continue

            orkg_resources[resource_id] = orkg_resource

        if orkg_resources[resource_id].content and "label" in orkg_resources[resource_id].content:
            path_labels.append(orkg_resources[resource_id].content["label"])

    if len(path) != len(path_labels):
        return []

    return path_labels


def recompute_similar_predicates(predicate_contributions: List[List[Dict]]) -> List[str]:
    return list(
        {
            target["pathLabels"][-1]
            for contribution_targets in predicate_contributions
            for target in contribution_targets
            if "pathLabels" in target
        }
    )


def adapt_comparison_schema(source_comparison) -> Optional[Dict[str, Any]]:
    comparison = {
        "contributions": [
            {
                "id": contribution["id"],
                "label": contribution["contributionLabel"],
                "paper_id": contribution["paperId"],
                "paper_label": contribution["title"],
                "paper_year": contribution.get("year"),
            }
            if contribution
            else {}
            for contribution in source_comparison["contributions"]
        ],
        "predicates": [
            {
                "id": predicate["id"],
                "label": predicate["label"],
                "n_contributions": predicate["contributionAmount"],
                "active": predicate["active"],
                "similar_predicates": [
                    similar
                    for similar in recompute_similar_predicates(
                        source_comparison["data"][predicate["id"]]
                    )
                    if similar != predicate["id"]
                ],
            }
            for predicate in source_comparison["properties"]
        ],
        "data": {
            predicate_id: [
                [
                    {
                        "id": target["resourceId"],
                        "label": target["label"],
                        "_class": target["type"],
                        "classes": target.get("classes", []),
                        "path": target["path"],
                        "path_labels": target.get("pathLabels", []),
                    }
                    if target
                    else {}
                    for target in contribution_targets
                ]
                for contribution_targets in predicate_contributions
            ]
            for predicate_id, predicate_contributions in source_comparison["data"].items()
        },
    }

    # check contributions details are there
    for contribution in comparison["contributions"]:
        if not contribution:
            return None

    # check predicates have information for all contributions
    for _, predicate_contributions in comparison["data"].items():
        if len(predicate_contributions) != len(comparison["contributions"]):
            return None

    # sanitize path_labels and classes
    for _, predicate_contributions in comparison["data"].items():
        for contribution in predicate_contributions:
            for target in contribution:
                if not target:
                    continue

                if len(target["path"]) != len(target["path_labels"]):
                    logger.debug(
                        f"Getting path_labels for id={target['id']}, path={target['path']}"
                    )
                    target["path_labels"] = get_path_labels(target["path"])

                if not target["classes"]:
                    logger.debug(f"Getting classes for {target['id']}")
                    target["classes"] = get_classes(target["id"])

    return comparison


def create_thing_by_type(
    source_session: Session, source_thing: source_models.Base, thing_type: str
) -> Optional[destination_models.Thing]:
    if "COMPARISON" == thing_type:
        try:
            _, config = parse_config(source_thing.data["url"])
        except KeyError as e:
            warning(
                ISSUES.THING_CONFIG_CANNOT_BE_PARSED,
                (source_thing.id, source_thing.resource_id),
                source_thing,
                e,
            )
            return None

        if not config["response_hash"]:
            warning(
                ISSUES.THING_HAS_NO_RESPONSE_HASH,
                (source_thing.id, source_thing.resource_id),
                source_thing,
            )
            return recompute_thing(config, source_thing)

        source_comparison = (
            source_session.query(source_models.Base.metadata.tables["comparison_responses"])
            .filter_by(response_hash=config["response_hash"])
            .first()
        )

        if not source_comparison:
            warning(
                ISSUES.THING_HAS_NO_COMPARISON_DATA,
                (source_thing.id, source_thing.resource_id),
                source_thing,
            )
            return None

        if isinstance(source_comparison.data, dict):
            source_comparison = source_comparison.data
        elif isinstance(source_comparison.data, str):
            source_comparison = json.loads(source_comparison.data)

        if config["type"] is None:
            if all([p["id"] == p["label"] for p in source_comparison["properties"]]):
                config["type"] = "PATH"
            else:
                config["type"] = "MERGE"

        data = adapt_comparison_schema(source_comparison)

        if not data:
            warning(
                ISSUES.PROBLEM_WHILE_ADAPTING_COMPARISON_SCHEMA,
                (source_thing.id, source_thing.resource_id),
                source_comparison,
            )
            data = recompute_comparison(config, source_thing)

            if not data:
                return None

    elif "DRAFT_COMPARISON" == thing_type:
        try:
            _, config = parse_config(source_thing.data["url"])
        except KeyError as e:
            warning(
                ISSUES.THING_CONFIG_CANNOT_BE_PARSED,
                (source_thing.id, source_thing.resource_id),
                source_thing,
                e,
            )
            return None

        data = {}
    else:
        config = {}
        data = source_thing.data

    thing = destination_models.Thing(
        created_at=source_thing.created_at,
        updated_at=source_thing.updated_at,
        thing_type=thing_type,
        thing_key=source_thing.resource_id,
        data=data,
        config=config,
    )

    return thing


def choose_appropriate_thing(
    things: List[destination_models.Thing], config: Dict[str, Any], source_link
):
    def compare(value_1: Union[str, bool, List, None], value_2: Union[str, bool, List, None]):
        if type(value_1) != type(value_2):
            return False

        if isinstance(value_1, (str, bool, type(None))):
            return value_1 == value_2

        if isinstance(value_1, List):
            return set(value_1) == set(value_2)

        warning(ISSUES.UNKNOWN_CONFIG_FIELD_TYPE, None, value_1, value_2)
        return False

    for thing in things:
        if all(
            [
                compare(config[key], value)
                for key, value in thing.config.items()
                if key not in ["type", "short_codes"]
                # we cannot curate the type at this point yet, therefore, we can safely ignore it.
                # short_codes might have been already added to one of the things via a previous link,
                # which cannot be found in a config object obtained from a later link.
            ]
        ):
            thing.config = {
                **thing.config,
                "short_codes": [*thing.config.get("short_codes", []), source_link.short_code],
            }
            return thing

    thing = destination_models.Thing(
        created_at=source_link.created_at,
        updated_at=source_link.updated_at,
        thing_type="COMPARISON",
        thing_key=source_link.short_code,
        data=things[0].data,  # we are sure the response_hash is equal :)
        config=config,
    )

    warning(
        ISSUES.LINK_POINTS_TO_MULTIPLE_THINGS,
        (source_link.id, source_link.short_code),
        source_link,
        thing,
    )
    return thing


def migrate_visualization_models(
    source_session: Session, destination_session: Session, source_thing: Any, constraints: List
):
    try:
        orkg_resource = orkg.resources.by_id(id=source_thing.resource_id)
    except Exception as e:
        warning(ISSUES.ORKG_BACKEND_EXCEPTION, e, e)
        return

    if orkg_resource.status_code == http.HTTPStatus.NOT_FOUND:
        warning(
            ISSUES.THING_COULD_NOT_BE_FOUND_IN_THE_GRAPH,
            (source_thing.id, source_thing.resource_id),
            source_thing,
        )
        return

    if orkg_resource.status_code != http.HTTPStatus.OK:
        warning(
            ISSUES.ERROR_WHILE_RETRIEVING_THING_FROM_ORKG,
            (source_thing.id, source_thing.resource_id),
            source_thing,
        )
        return

    if source_thing.resource_id not in orkg_resources:
        orkg_resources[source_thing.resource_id] = orkg_resource

    thing_type = detect_thing_type(orkg_resource.content["classes"])

    if not thing_type:
        warning(
            ISSUES.THING_TYPE_UNDETECTED,
            (source_thing.id, source_thing.resource_id),
            source_thing,
        )
        return

    if (thing_type, source_thing.resource_id) in constraints:
        warning(ISSUES.THING_DUPLICATED, (source_thing.id, source_thing.resource_id), source_thing)
        return

    thing = create_thing_by_type(source_session, source_thing, thing_type)

    if not thing:
        return

    if thing_type == "COMPARISON":
        if thing.config["response_hash"] not in migrated_response_hashes:
            migrated_response_hashes[thing.config["response_hash"]] = []

        migrated_response_hashes[thing.config["response_hash"]].append(thing)

    if thing_type in ["COMPARISON", "DRAFT_COMPARISON"]:
        del thing.config["response_hash"]

    logger.debug(f"Converting {source_thing} to {thing}")

    destination_session.add(thing)
    destination_session.commit()
    destination_session.refresh(thing)

    constraints.append((thing.thing_type, thing.thing_key))


def migrate_links(
    source_session: Session, destination_session: Session, source_link: Any, constraints: List
):
    if any(
        [
            substring in source_link.long_url
            for substring in ["localhost", ":comparisonId", "/paper"]
        ]
    ):
        warning(
            ISSUES.LINK_SCHEMA_INCORRECT, (source_link.id, source_link.short_code), source_link
        )
        return

    comparison_id, config = parse_config(source_link.long_url, is_link=True)
    if comparison_id:
        thing = (
            destination_session.query(destination_models.Thing)
            .filter_by(thing_key=comparison_id)
            .first()
        )

        if not thing:
            warning(
                ISSUES.LINK_POINTS_TO_UNPUBLISHED_COMPARISON,
                (source_link.id, source_link.short_code),
                source_link,
            )
            return

        thing.config = {
            **thing.config,
            "short_codes": [*thing.config.get("short_codes", []), source_link.short_code],
        }

        logger.debug(f"Converting {source_link} to {thing}")
        destination_session.commit()
        return

    if response_hash := config["response_hash"]:
        # published shared comparisons
        if response_hash in migrated_response_hashes:
            things = migrated_response_hashes[response_hash]
            thing = choose_appropriate_thing(things, config, source_link)

            logger.debug(f"Converting {source_link} to {thing}")

            if "short_codes" not in thing.config:
                # we created a new entry because of ambiguity
                # and the short_code can be found in the thing_key
                del thing.config["response_hash"]
                destination_session.add(thing)

            destination_session.commit()
            return

        # unpublished shared comparison
        else:
            thing_type = "COMPARISON"

            source_comparison = (
                source_session.query(source_models.Base.metadata.tables["comparison_responses"])
                .filter_by(response_hash=response_hash)
                .first()
            )

            if not source_comparison:
                warning(
                    ISSUES.LINK_HAS_NO_COMPARISON_DATA,
                    (source_link.id, source_link.short_code),
                    source_link,
                )
                return

            if isinstance(source_comparison.data, dict):
                source_comparison = source_comparison.data
            elif isinstance(source_comparison.data, str):
                source_comparison = json.loads(source_comparison.data)

            data = adapt_comparison_schema(source_comparison)

            if not data:
                warning(
                    ISSUES.PROBLEM_WHILE_ADAPTING_COMPARISON_SCHEMA,
                    (source_link.id, source_link.short_code),
                    source_comparison,
                )
                data = recompute_comparison(config, source_link)

                if not data:
                    return
    # unpublished shared without data comparison
    else:
        thing_type = "DRAFT_COMPARISON"
        data = {}

    del config["response_hash"]
    thing = destination_models.Thing(
        created_at=source_link.created_at,
        updated_at=source_link.updated_at,
        thing_type=thing_type,
        thing_key=source_link.short_code,
        data=data,
        config=config,
    )

    logger.debug(f"Converting {source_link} to {thing}")

    destination_session.add(thing)
    destination_session.commit()
    destination_session.refresh(thing)


def main():
    print(
        "Migrating data from '{}' to '{}'".format(
            os.getenv("ORKG_SIMILARITY_DATABASE_URI"), os.getenv("ORKG_SIMCOMP_API_DATABASE_URI")
        )
    )

    if input("Do you want to continue ? (y/n): ").lower() != "y":
        exit(0)

    source_session = create_db_connection(
        os.getenv("ORKG_SIMILARITY_DATABASE_URI"), source_models.Base, logging_name="source"
    )

    destination_session = create_db_connection(
        os.getenv("ORKG_SIMCOMP_API_DATABASE_URI"),
        destination_models.Base,
        logging_name="destination",
    )

    migration_tables = [
        {
            "entity_name": "thing",
            "table_name": "visualization_models",
            "migration_func": migrate_visualization_models,
            "issues": VisualizationIssues,
        },
        {
            "entity_name": "link",
            "table_name": "links",
            "migration_func": migrate_links,
            "issues": LinksIssues,
        },
    ]

    try:
        for table in migration_tables:
            logger.debug("#-------------------------------------------------------#")
            logger.debug(f"Migrating '{table['table_name']}' table")

            source_entities = source_session.query(
                source_models.Base.metadata.tables[table["table_name"]]
            ).all()

            constraints = []
            for i, source_entity in enumerate(source_entities):
                logger.info(
                    f"{i + 1}/{len(source_entities)}"
                    f" - Attempting to migrate {table['entity_name']}"
                    f" with ID: {source_entity.id}"
                )

                try:
                    table["migration_func"](
                        source_session, destination_session, source_entity, constraints
                    )
                except Exception as e:
                    warning(ISSUES.UNEXPECTED_EXCEPTION, (source_entity.id, e), e)
                    traceback.print_exc()
                    continue

            report(table, len(source_entities))
            logger.debug("#-------------------------------------------------------#")
    finally:
        source_session.close()
        destination_session.close()


if __name__ == "__main__":
    main()

# -*- coding: utf-8 -*-
from datetime import datetime
from uuid import uuid4

from sqlalchemy import JSON, Column, DateTime, String, UniqueConstraint
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base(name="DestinationBase")


class BaseTable:
    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        default=uuid4,
    )
    created_at = Column(
        DateTime(timezone=True),
        default=datetime.utcnow,
    )
    updated_at = Column(
        DateTime(timezone=True),
        default=datetime.utcnow,
    )

    def __repr__(self):
        return " ".join(
            [
                "{}={}".format(key, value)
                for key, value in self.__dict__.items()
                if not key.startswith("_")
            ]
        )


class Thing(Base, BaseTable):
    __tablename__ = "things"

    thing_type = Column(String, nullable=False)
    thing_key = Column(String, nullable=False)
    data = Column(JSON, nullable=False)
    config = Column(JSON, nullable=False)

    __table_args__ = (
        UniqueConstraint(
            "thing_type",
            "thing_key",
            name="_type_key_unique_constraint",
        ),
    )

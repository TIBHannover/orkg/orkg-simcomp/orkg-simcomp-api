#!/bin/sh

ENV_PATH=.env
DUMP_PATH=./dumps/orkg-similarity-backup-2023-03-13.sql

if ! [ -e $ENV_PATH ]
then
    echo "Please create an '.env' file in this directory and provide the needed env variables to run the script."
    exit
fi


echo "Loading the following environment variables:"
export $(grep -v '^#' $ENV_PATH | xargs -d '\n')
grep -v '^#' $ENV_PATH
echo ""

if command docker-compose > /dev/null 2>&1
then
  DOCKER_COMPOSE_CMD="docker-compose"
elif command docker compose > /dev/null 2>&1
then
  DOCKER_COMPOSE_CMD="docker compose"
else
  echo "docker-compose or docker compose is not installed on this machine"
  exit 1
fi

$DOCKER_COMPOSE_CMD up --build -d
echo ""

echo "Cleaning the database '$ORKG_SIMILARITY_POSTGRES_DB'"
echo "DROP SCHEMA public CASCADE; CREATE SCHEMA public;" \
    | docker exec -i orkg_similarity_migration_postgres_container \
    psql -U $ORKG_SIMILARITY_POSTGRES_USER
echo ""

echo "Importing the database dump '$DUMP_PATH' to database '$ORKG_SIMILARITY_POSTGRES_DB'"
docker exec -i orkg_similarity_migration_postgres_container \
    psql -U $ORKG_SIMILARITY_POSTGRES_USER < $DUMP_PATH
echo ""

echo "Migrating..."
python -m src.migrate
echo ""

echo "Exporting the database '$ORKG_SIMCOMP_API_POSTGRES_DB'"
docker exec -t orkg_simcomp_migration_postgres_container \
    pg_dumpall -c -U $ORKG_SIMCOMP_API_POSTGRES_USER \
    > ./dumps/orkg-simcomp-api-backup-`date +%Y-%m-%d`.sql
echo ""

$DOCKER_COMPOSE_CMD stop

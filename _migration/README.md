# Data Migration from `orkg-similarity` to `orkg-simcomp-api`

We here document the database changes we need to consider while migration from the
**recent** [`orkg-similarity`](https://gitlab.com/TIBHannover/orkg/orkg-similarity) app
to the **current** [`orkg-simcomp-api`](https://gitlab.com/TIBHannover/orkg/orkg-simcomp/orkg-simcomp-api)
one.

## How to run

1. Make sure your terminal is running a python environment with `sqlalchemy` and `python-dotenv`.
2. Make sure you store your `legacy` database dump under `_migration/dumps/orkg-similarity-backup-some-date.sql`.
3. Change the `DUMP_PATH` variable in `_migration/migrate.sh` to match the name of your dump.
4. Create a `.env` file with the required environment variables.
5. Make sure you have changed your working directory to `_migration` directory via `cd _migration`.
6. Run the following commands:

```bash
chmod u+x ./migrate.sh
./migrate.sh
```

7. You will find the exported DB file under `_migration/dumps/orkg-simcomp-api-backup-some-date.sql`

## Environment Variables


| Variable                             | Description                                                                                         |
|--------------------------------------|-----------------------------------------------------------------------------------------------------|
| `ORKG_SIMILARITY_DATABASE_URI`       | `Source` DB URI.                                                                                    |
| `ORKG_SIMILARITY_POSTGRES_USER`      | `Source` DB username.                                                                               |
| `ORKG_SIMILARITY_POSTGRES_PASSWORD`  | `Source` DB password.                                                                               |
| `ORKG_SIMILARITY_POSTGRES_DB`        | `Source` DB name.                                                                                   |
| `ORKG_SIMILARITY_POSTGRES_PORT`      | `Source` DB port.                                                                                   |
| `ORKG_SIMCOMP_API_DATABASE_URI`      | `Destination` DB URI.                                                                               |
| `ORKG_SIMCOMP_API_POSTGRES_USER`     | `Destination` DB username.                                                                          |
| `ORKG_SIMCOMP_API_POSTGRES_PASSWORD` | `Destination` DB password.                                                                          |
| `ORKG_SIMCOMP_API_POSTGRES_DB`       | `Destination` DB name.                                                                              |
| `ORKG_SIMCOMP_API_POSTGRES_PORT`     | `Destination` DB port.                                                                              |
| `ORKG_BACKEND_API_HOST`              | Host URL of the ORKG-Backend API. Recommended to have the same value as in `orkg-simcomp-api/.env`. |


## Transformation of `Things`

> Note: Fields denoted with `// optional` are optional fields.


### Other ThingTypes

We did not change the schemas of the objects that the new simcomp service doesn't produce. Below are the changes of the comparison object.


### `COMPARISON`

### orkg-similarity

```json
{
   "contributions": [
     {
       "contributionLabel": "",
       "id": "",
       "paperId": "",
       "title": "",
       "year": "" // optional
     },
     ...
   ],
   "properties": [
     {
       "active": true,
       "contributionAmount": 3,
       "id": "",
       "label": ""
     },
     ...
   ],
   "data": {
     "arbitrary_id": [
       [
         {
           "label": "",
           "path": ["", ...],
           "pathLabels": ["", ...], // optional
           "classes": ["", ...], // optional
           "resourceId": "",
           "type": ""
         },
         ...
       ],
       ...
     ] ,
     ...
   }
}
```

### orkg-simcomp-api

```json
{
   "contributions": [
     { // has to be there
       "id": "",
       "label": "",
       "paper_id": "",
       "paper_label": "",
       "paper_year": "" // can be empty
     },
     ...
   ],
   "predicates": [
     { // has to be there
       "id": "",
       "label": "",
       "n_contributions": 3,
       "active": true,
       "similar_predicates": ["", ...] // can be empty
     },
     ...
   ],
   "data": {
     "arbitrary_id": [
       [ // can be empty
         { // can be empty
           "id": "",
           "label": "",
           "_class": "",
           "classes": ["", ...], // can be empty
           "path": ["", ...],
           "path_labels": ["", ...] // can be empty.
         },
         ...
       ],
       ...
     ] ,
     ...
   }
}
```

and its config is
```json
{
  "predicates": ["", ...], // empty list means no filtering, which is equivalent to the full list of predicates.
  "contributions": ["", ...],
  "transpose": true,
  "short_code": "", // only for shared comparisons.
  "type": ""
}
```

## Key Changes by Table

### `links` Table

* completely removed.
* a `short_code` can be passed as a value for `thing_key` and the relevant comparison will be retrieved.
* published shared links: We will extend the corresponding thing[COMPARISON] config with short_code.
* unpublished shared links with response_hash: we will create new thing[COMPARISON] entry with short_code in the config.
* unpublished shared links without response_hash: we will create a new thing[DRAF_COMPARISON] entry with short_code in the config and without data.

### `visualization_models` Table

#### Recent
* `resource_id`
* `data`

#### Current renamed to `things`
* `thing_type`
* `thing_id`
* `data`
* `config`
* UniqueConstraint has been added for (thing_type and thing_id)
* the config field is currently important for the transition phase, since some configs are still being stored in the DB
    from the frontend side. This field should be removed in the future and configs will be directly reflected in the
    data object.

### `comparison_responses` Table

* Completely removed.
* `response_hash` and `save_response` parameters removed from the comparison service.
* Instead, `POST and GET /thing` will be used for saving and fetching a published
    comparison. The `thing_key` will have the value of resource_id for published ones and
    the short_code for unpublished ones for backward compatibility. Published and shared ones
    will have their short_code mentioned in the Thing.config object. Frontend team will disable
    the share link feature.
* `GET /thing/export` is also provided to export different things based on their type and the desired format.


## Notes to the Frontend Team

* Please check our OpenAPI specification for changed routes and request/response schemas.
* You can use the response status codes or suggest others to deal with our responses.
* All responses have a uuid, timestamp, and a payload object, in which you will find what you are
    (hopefully) looking for.
* Your work will base on either contributions (get similarities / compare them) or things (add / get / export).
* Things are typed with defined types that the orkg-simcomp's team agrees on with you.
* ThingTypes are (currently) useful for exporting things in predefined formats.
* Reviews don't have their standalone service anymore. Rather, a review is now a Thing with the type REVIEW that can
    be exported with the XML format.
* If you are looking for a link via a short_code, pass it as a thing_key to the things getting service, and you'll
    get the corresponding data directly ;)
* Saving a comparison can be done by computing it first and then saving it as a Thing typed COMPARISON.
* The concept of the response_hash is completely removed. Identification, data and configuration are all stored
    in the Things table, and can be grouped by thing_type ;)
* A Thing has a config object (for backward compatibility for now). Its fields have to be agreed on, please do NOT
    store anything without getting back to us.
* Currently, we only use the config of a COMPARISON thing to be able to export it based on the stored predicates list
    (recall the like_ui logic).
* We use `snake_case` naming convention for fields of self-produced objects (e.g. comparison) and
  undefined naming convention for others (e.g. review). Thus, we leave a free room for the frontend team to store
  things in the way they wish.
* Do NOT ever hesitate to get in touch in case anything is unclear!! :)

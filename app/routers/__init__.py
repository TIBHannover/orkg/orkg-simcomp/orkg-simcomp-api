# -*- coding: utf-8 -*-
import os
from functools import wraps

from fastapi import HTTPException, Request, status


def check_security_headers(header_name: str):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # `Request` object is passed through FastAPI's dependency injection
            request: Request = kwargs.get("request")

            # Check if the required header is present and has the expected value
            expected_value = os.getenv("ORKG_SIMCOMP_ACCESS_KEY", None)
            if expected_value is None:
                raise HTTPException(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail="Internal server error: Missing ORKG_SIMCOMP_ACCESS_KEY environment variable",
                )

            if (
                header_name not in request.headers
                or request.headers[header_name] != expected_value
            ):
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail=f"Unauthorized: Missing or incorrect {header_name} header",
                )
            # If check passes, call the original function
            return func(*args, **kwargs)

        return wrapper

    return decorator

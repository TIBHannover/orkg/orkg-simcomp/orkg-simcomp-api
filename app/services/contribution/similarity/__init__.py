# -*- coding: utf-8 -*-
from app.services.contribution.similarity.service import ContributionSimilarityService

__all__ = ["ContributionSimilarityService"]

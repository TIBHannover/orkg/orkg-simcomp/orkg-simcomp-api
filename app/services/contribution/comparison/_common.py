# -*- coding: utf-8 -*-
from typing import List, Optional

from app.models.contribution import ComparisonHeaderCell
from app.services.common.orkg_backend import OrkgBackendWrapperService


def get_contributions_details(
    orkg_backend: OrkgBackendWrapperService,
    contribution_ids: List[str],
) -> List[Optional[ComparisonHeaderCell]]:
    # TODO: call orkg_backend.get_contribution_details in case len(contribution_ids) == 1

    contributions_details = orkg_backend.get_contributions_details(contribution_ids)

    if not contributions_details:
        return []

    result = []
    for contribution_id in contribution_ids:
        contribution_details = [
            contribution_details
            for contribution_details in contributions_details
            if contribution_details and contribution_details["id"] == contribution_id
        ]

        if contribution_details:
            result.append(
                ComparisonHeaderCell(
                    id=contribution_id,
                    label=contribution_details[0]["label"],
                    paper_id=contribution_details[0]["paper_id"],
                    # TODO: rename paper_title to paper_label in backend
                    paper_label=contribution_details[0]["paper_title"],
                    paper_year=contribution_details[0]["paper_year"],
                )
            )
        else:
            result.append(None)

    return result


def clean_classes(
    classes: List[str],
) -> List[str]:
    return [
        c
        for c in classes
        if c
        not in [
            "Thing",
            "Literal",
            "AuditableEntity",  # TODO: remove this when it is removed from the graph
            "Resource",
        ]
    ]

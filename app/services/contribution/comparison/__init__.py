# -*- coding: utf-8 -*-
from app.services.contribution.comparison.service import ContributionComparisonService

__all__ = ["ContributionComparisonService"]

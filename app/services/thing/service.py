# -*- coding: utf-8 -*-
import http
import os
from typing import Any, Dict

from app.common.errors import OrkgSimCompApiError
from app.db.crud import CRUDService
from app.db.models.thing import Thing
from app.models.thing import ExportFormat, ThingType
from app.services.common.base import OrkgSimCompApiService
from app.services.thing.export import ComparisonExporter, ReviewExporter


class ThingService(OrkgSimCompApiService):
    def __init__(self, crud_service: CRUDService):
        super().__init__(logger_name=__name__)

        self.crud_service = crud_service
        self.typed_exporters = {
            ThingType.COMPARISON: ComparisonExporter,
            ThingType.REVIEW: ReviewExporter,
        }

    def add_thing(
        self, thing_type: ThingType, thing_key: str, data: Dict[str, Any], config: Dict[str, Any]
    ):
        if thing_type == ThingType.UNKNOWN and os.environ.get("ORKG_SIMCOMP_API_ENV") != "test":
            raise OrkgSimCompApiError(
                message='thing_type="{}" is only allowed for test usage.'.format(thing_type),
                cls=self.__class__,
                status_code=http.HTTPStatus.BAD_REQUEST,
            )

        thing = self.crud_service.get_row_by(
            entity=Thing,
            columns_values={
                "thing_type": thing_type,
                "thing_key": thing_key,
            },
        )

        if thing:
            raise OrkgSimCompApiError(
                message='Thing with thing_type="{}" and thing_key="{}" already exists.'.format(
                    thing_type, thing_key
                ),
                cls=self.__class__,
                status_code=http.HTTPStatus.CONFLICT,
            )

        if thing_type in self.typed_exporters:
            try:
                self.typed_exporters[thing_type].parse(data)
            except OrkgSimCompApiError:
                raise OrkgSimCompApiError(
                    message="Data object cannot be parsed as a {}".format(thing_type),
                    cls=self.__class__,
                    status_code=http.HTTPStatus.BAD_REQUEST,
                )

        thing = Thing(thing_type=thing_type, thing_key=thing_key, data=data, config=config)
        self.crud_service.create(entity=thing)

    def replace_thing(
        self, thing_type: ThingType, thing_key: str, data: Dict[str, Any], config: Dict[str, Any]
    ) -> bool:
        if thing_type == ThingType.UNKNOWN and os.environ.get("ORKG_SIMCOMP_API_ENV") != "test":
            raise OrkgSimCompApiError(
                message='thing_type="{}" is only allowed for test usage.'.format(thing_type),
                cls=self.__class__,
                status_code=http.HTTPStatus.BAD_REQUEST,
            )

        if (
            thing_type != ThingType.DRAFT_COMPARISON
            and os.environ.get("ORKG_SIMCOMP_API_ENV") != "test"
        ):
            raise OrkgSimCompApiError(
                message='thing_type="{}" is the only editable type.'.format(thing_type),
                cls=self.__class__,
                status_code=http.HTTPStatus.BAD_REQUEST,
            )

        thing = self.crud_service.get_row_by(
            entity=Thing,
            columns_values={
                "thing_type": thing_type,
                "thing_key": thing_key,
            },
        )

        if not thing:
            return False

        thing.data = data
        thing.config = config
        self.crud_service.update(entity=thing)
        return True

    def get_thing(
        self,
        thing_type: ThingType,
        thing_key: str,
    ):
        thing = self.crud_service.get_row_by(
            entity=Thing,
            columns_values={
                "thing_type": thing_type,
                "thing_key": thing_key,
            },
        )

        if thing:
            return {"thing": thing}

        if thing_type == ThingType.COMPARISON:
            # During the migration we replaced shared links of published comparisons
            # with the "short_code" field in the Thing.config. For backward compatibility,
            # getting a Thing[COMPARISON] can be done either by
            # 1. thing_key=resource_id,
            # 2. thing_key=short_code,
            # 3. or short_code in Thing.config["short_codes"].

            thing = self.crud_service.get_row_by_json_field(
                entity=Thing, json_field=Thing.config["short_codes"], value=thing_key
            )

            if thing:
                return {"thing": thing}

        raise OrkgSimCompApiError(
            message='Thing with thing_type="{}" and thing_key="{}" not found.'.format(
                thing_type, thing_key
            ),
            cls=self.__class__,
            status_code=http.HTTPStatus.NOT_FOUND,
        )

    def export_thing(self, thing_type: ThingType, thing_key: str, format: ExportFormat, **kwargs):
        thing = self.get_thing(thing_type, thing_key)["thing"]

        try:
            return self.typed_exporters[thing_type].export(
                thing.data, format, thing.config, self, **kwargs
            )
        except KeyError:
            raise OrkgSimCompApiError(
                message='Exporting thing with thing_type="{}" is not supported'.format(thing_type),
                cls=self.__class__,
                status_code=http.HTTPStatus.NOT_IMPLEMENTED,
            )

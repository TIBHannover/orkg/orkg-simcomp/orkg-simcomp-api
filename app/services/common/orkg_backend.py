# -*- coding: utf-8 -*-
import os
from typing import Any, Callable, List, Optional, Tuple, Union

import networkx as nx
import orkg
from orkg import ORKG

from app.services.common.base import OrkgSimCompApiService


class OrkgBackendWrapperService(OrkgSimCompApiService):
    def __init__(
        self,
        host: str = os.getenv(
            "ORKG_BACKEND_API_HOST",
            "http://localhost",
        ),
    ):
        super().__init__(logger_name=__name__)

        self.connector = ORKG(host=host)

    @staticmethod
    def get_instance():
        yield OrkgBackendWrapperService()

    def get_contribution_ids(self) -> List[str]:
        self.logger.debug("Getting all contributions in the ORKG ...")

        contributions = self._call_pageable(
            self.connector.classes.get_resource_by_class,
            args={"class_id": "Contribution"},
            params={
                "sort": True,
                "page": 0,
                "size": 300,
            },
        )

        return [contribution["id"] for contribution in contributions]

    def get_subgraph(
        self,
        thing_id: str,
        blacklist: Union[str, List[str]] = "ResearchField",
        max_level: int = -1,
    ) -> Optional[nx.DiGraph]:
        self.logger.debug("Getting subgraph for thing_id={}".format(thing_id))

        try:
            graph = orkg.subgraph(
                client=self.connector,
                thing_id=thing_id,
                blacklist=blacklist,
                max_level=max_level,
            )
        except ValueError:
            self.logger.warning(
                "An error occurred while obtaining a subgraph representation for {}".format(
                    thing_id
                )
            )
            return None

        return graph

    def get_contribution_details(self, contribution_id: str) -> dict:
        # TODO: use get_contributions_details instead
        self.logger.debug("Getting details for contribution_id={}".format(contribution_id))

        response = self.connector.statements.get_by_object_and_predicate(
            object_id=contribution_id,
            predicate_id="P31",
        )

        if (
            not response.succeeded
            or not isinstance(response.content, list)
            or not len(response.content) == 1
        ):
            self.logger.warning(
                "An error occurred while calling orkg.statements.get_by_object_and_predicate()"
            )
            return {}

        statement = response.content[0]
        return {
            "label": statement["object"]["label"],
            "paper_id": statement["subject"]["id"],
            "paper_label": statement["subject"]["label"],
        }

    def get_contributions_details(self, contribution_ids: List[str]) -> List[dict]:
        self.logger.debug("Getting details for contribution_ids={}".format(contribution_ids))

        response = self.connector.contribution_comparisons.by_ids_unpaginated(ids=contribution_ids)

        if not response.all_succeeded or not isinstance(response.content, list):
            self.logger.warning(
                "An error occurred while calling orkg.contribution_comparisons.by_ids_unpaginated()"
            )
            return []

        return response.content

    def _call_pageable(
        self,
        func: Callable[..., Any],
        args: dict,
        params: dict,
    ) -> List[dict]:
        def _call_one_page(
            page: int,
        ) -> Tuple[int, List[dict]]:
            params["page"] = page
            response = func(**args, params=params)

            if not response.succeeded:
                self.logger.warning(
                    "An error occurred while calling {}(args={}, params={})".format(
                        func, args, params
                    )
                )
                return 0, []

            return (
                response.page_info["totalPages"],
                response.content,
            )

        all_items = []
        n_pages, items = _call_one_page(page=0)
        all_items.extend(items)

        for i in range(1, n_pages):
            _, items = _call_one_page(page=i)
            all_items.extend(items)

        return all_items

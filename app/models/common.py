# -*- coding: utf-8 -*-
from datetime import datetime
from typing import Any, List, Optional
from uuid import UUID

from pydantic import BaseModel, ConfigDict


class Response(BaseModel):
    timestamp: datetime
    uuid: UUID
    payload: Any = None


class Request(BaseModel):
    pass


class BaseORMObject(BaseModel):
    id: Optional[UUID] = None
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None
    model_config = ConfigDict(from_attributes=True)


class OrkgResource(BaseModel):
    created_at: datetime
    created_by: str
    id: str


class OrkgObject(OrkgResource):
    datatype: Optional[str] = None
    label: str
    classes: Optional[List[str]] = None


class OrkgPredicate(OrkgResource):
    label: str
    description: Optional[str] = None

    def __init__(self, **data):
        super().__init__(**data)
        self._class = "predicate"


class OrkgSubject(OrkgResource):
    classes: Optional[List[str]] = None
    extraction_method: Optional[str] = None
    featured: Optional[bool] = None
    formatted_label: Optional[str] = None
    label: str
    observatory_id: Optional[str] = None
    organization_id: Optional[str] = None
    shared: Optional[int] = None
    unlisted: Optional[bool] = None
    verified: Optional[bool] = None

    def __init__(self, **data):
        super().__init__(**data)
        self._class = "resource"


class OrkgStatement(OrkgResource):
    object: OrkgObject
    predicate: OrkgPredicate
    subject: OrkgSubject

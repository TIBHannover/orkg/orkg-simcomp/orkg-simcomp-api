# -*- coding: utf-8 -*-
from enum import Enum
from typing import Dict, List, Optional, Union

from pydantic import BaseModel, ConfigDict

from app.models.common import Response


class ContributionSimilarityInitIndexResponse(Response):
    class Payload(BaseModel):
        n_contributions: int
        n_indexed_contributions: int
        not_indexed_contributions: List[str]

    payload: Payload


class ContributionSimilarityIndexResponse(Response):
    class Payload(BaseModel):
        message: str

    payload: Payload


class ContributionSimilaritySimilarResponse(Response):
    class Payload(BaseModel):
        class SimilarContribution(BaseModel):
            id: str
            label: Optional[str] = None
            paper_id: Optional[str] = None
            paper_label: Optional[str] = None
            similarity_percentage: float

        contributions: List[SimilarContribution]

    payload: Payload


class ComparisonType(str, Enum):
    PATH = "PATH"
    MERGE = "MERGE"


class ComparisonHeaderCell(BaseModel):
    id: str
    label: str
    paper_id: str
    paper_label: str
    paper_year: Optional[str | int] = None


class ComparisonIndexCell(BaseModel):
    id: str
    label: str
    n_contributions: int
    active: bool
    similar_predicates: Optional[List[str]] = None


class ComparisonTargetCell(BaseModel):
    id: str
    label: str
    classes: List[str]
    path: List[str]
    path_labels: List[str]

    model_config = ConfigDict(extra="allow")

    def __init__(self, **data):
        super().__init__(**data)
        self._class = data.get("_class", "resource")


class Comparison(BaseModel):
    contributions: List[ComparisonHeaderCell] = []
    predicates: List[ComparisonIndexCell] = []
    data: Dict[
        str,
        List[List[Union[ComparisonTargetCell, dict]]],
    ] = {}


class ContributionComparisonResponse(Response):
    class Payload(BaseModel):
        comparison: Comparison

    payload: Payload

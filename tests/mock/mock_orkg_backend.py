# -*- coding: utf-8 -*-
import networkx as nx


class OrkgBackendWrapperServiceMock:
    def __init__(self):
        self.contributions = {
            "123": {
                "subgraph": self.__create_subgraph("123"),
                "details": {
                    "id": "123",
                    "label": "test label 1",
                    "paper_id": "test paper_id 1",
                    "paper_title": "test paper_label 1",
                    "paper_year": "2007",
                },
            },
            "234": {
                "subgraph": self.__create_subgraph("234"),
                "details": {
                    "id": "234",
                    "label": "test label 2",
                    "paper_id": "test paper_id 2",
                    "paper_title": "test paper_label 2",
                    "paper_year": "2007",
                },
            },
            "345": {
                "subgraph": self.__create_subgraph("345"),
                "details": {
                    "id": "345",
                    "label": "test label 3",
                    "paper_id": "test paper_id 3",
                    "paper_title": "test paper_label 3",
                    "paper_year": "2007",
                },
            },
        }

        self.papers = {"test paper_id": {"year": "2007"}}

    @staticmethod
    def get_instance():
        yield OrkgBackendWrapperServiceMock()

    def get_contribution_ids(self):
        return list(self.contributions.keys())

    def get_subgraph(self, thing_id):
        return self.contributions.get(thing_id, {}).get("subgraph", None)

    def get_contribution_details(self, contribution_id):
        # TODO: use get_contributions_details instead
        # TODO: rename paper_title to paper_label in backend

        details = self.contributions.get(contribution_id, {}).get("details", None)

        details["paper_label"] = details["paper_title"]
        del details["paper_title"]

        return details

    def get_contributions_details(self, contribution_ids):
        return [
            self.contributions.get(contribution_id, {}).get("details", None)
            for contribution_id in contribution_ids
        ]

    def get_paper_year(self, paper_id):
        return self.papers.get(paper_id, {}).get("year", None)

    @staticmethod
    def __create_subgraph(thing_id):
        subgraph = nx.DiGraph()

        subgraph.add_node(
            node_for_adding=thing_id,
            id=thing_id,
            label="Contribution {}".format(thing_id),
            _class="resource",
        )
        subgraph.add_node(
            node_for_adding=thing_id + " target",
            id=thing_id + " target",
            label="Contribution {}".format(thing_id),
            _class="literal",
        )
        subgraph.add_edge(
            u_of_edge=thing_id,
            v_of_edge=thing_id + " target",
            id=thing_id + thing_id + " target",
            label="Predicate {}".format(thing_id),
        )

        return subgraph
